# Frontend Mentor - Photosnap Website solution

This is a solution to the [Photosnap Website challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/photosnap-multipage-website-nMDSrNmNW). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
  - [Screenshots](#screenshots)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for each page depending on their device's screen size
- See hover states for all interactive elements throughout the site

### Links

- Solution URL: [Click here to see the code](https://gitlab.com/ELeonhardt/photosnap-website)
- Live Site URL: [Click here to see the working project](https://photosnap-website.vercel.app/)

### Screenshots

![](./screenshot1.png)
![](./screenshot2.png)
![](./screenshot3.png)
![](./screenshot4.png)
![](./screenshot5.png)

## My process

### Built with

- Semantic HTML5 markup
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Gatsby.js](https://www.gatsbyjs.com/) - React framework
- [Styled Components](https://styled-components.com/) - For styles
- [Sanity CMS](https://www.sanity.io/) - For storing story data and images

### What I learned

- The project was ver useful to review css-grid, I even used subgrid in the features page.
- The project has a lot of :before and :after pseudoelements with gradient backgrounds. I repeated them a lot, which was a great practice exercise.
- The project has a lot of reusable components, which made development very pleasurable and it forced me to think about reusability to keep my codebase small.
- It was the first for me using a real design file. I used Figma and absolutely loved how easy it was to see font-sizes, colors, heights of elements etc.
- It was the fist time that I styled some svgs with css. I now feel a lot more confident with svg in general.
- I took the time to look deeply into how to create responsive navbars and how is the best way to make them appear on mobile screens.

### Continued development

While coding the project, I had the misconception that images have to go through sanity to be optimized. I have since learned more about gatsby-image-plugin and started optimizing static images which will always be in the same place in a less awkward way, and not pulling them from a CMS. For this project, which at the end is a static site, I would not use Sanity again, but gatsby-source-filesystem together with static and dynamic image tags provided by Gatsby for the best image optimization.

### Useful resources

- [Master Gatsby Source from Wes Bos](https://mastergatsby.com/) - I loved this course and it is worth every penny.
- [Responsive Navigation Bar Tutorial](https://www.youtube.com/watch?v=gXkqy0b4M5g) & [Create a responsive navigation nav with no JS!](https://www.youtube.com/watch?v=8QKOaTYvYUA) These are two great videos about creating responsive navbars. I decided to merge both methods and adapt them to React for my projects. The end result was a navbar using the React useState hook which uses scale to appear and disappear. Check out the Nav component in my code!

## Author

- Website - [Elisabeth Leonhardt](https://www.elisabethleonhardt.com/)
- Frontend Mentor - [@MiaSinger](https://www.frontendmentor.io/profile/MiaSinger)
