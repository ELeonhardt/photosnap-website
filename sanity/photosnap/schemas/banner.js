export default {
  title: 'Banner',
  name: 'banner',
  type: 'document',
  fields: [
    {
      title: 'Picture',
      name: 'picture',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      title: 'Title',
      name: 'title',
      type: 'string',
    },
    {
      title: 'Subtitle',
      name: 'subtitle',
      type: 'string',
    },
    {
      title: 'Date',
      name: 'date',
      type: 'string',
    },
    {
      name: 'shown',
      title: 'Shown on Page',
      type: 'string',
    },
    {
      title: 'Author',
      name: 'author',
      type: 'string',
    },
    {
      title: 'Text',
      name: 'text',
      type: 'text',
    },
  ],
};
