export default {
  name: 'introsection',
  title: 'Introsection',
  type: 'document',
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'text',
      title: 'text',
      type: 'text',
    },
    {
      name: 'shown',
      title: 'Shown on Page',
      type: 'string',
    },
    {
      name: 'picture',
      title: 'Picture',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
  ],
};
