import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import ArrowButton from './ArrowButton';

const StoryCardStyles = styled.div`
  height: 50rem;
  color: var(--pure-white);
  position: relative;
  transition: transform var(--transition-time) ease-in-out;

  .flex-container {
    position: absolute;
    left: 10%;
    bottom: 8%;
    width: 80%;
    display: flex;
    flex-direction: column;
    gap: 1.5rem;
  }

  .line {
    border-top: 1px solid rgba(255, 255, 255, 0.5);
  }

  &:after {
    content: '';
    position: absolute;
    width: 100%;
    height: 0px;
    background: linear-gradient(to right top, #ffc593, #bc7198, #5a77ff);
    bottom: 0;
    left: 0;
    transition: height var(--transition-time) ease-in-out;
  }

  &:hover {
    transform: translateY(-2.4rem);
  }

  &:hover:after {
    height: 6px;
  }

  @media (max-width: 430px) {
    height: 37.5rem;
  }
`;

const GatsbyImageStyles = styled(Img)`
  width: 100%;
  height: 100%;
  object-fit: cover;
  position: relative;

  &:after {
    content: '';
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background: rgba(0, 0, 0, 0.4);
    position: absolute;
  }
`;

function StoryCard({ title, date, author, picture }) {
  return (
    <StoryCardStyles>
      <GatsbyImageStyles fluid={picture.asset.fluid} />
      <div className="flex-container">
        <div>
          <p>{date}</p>
          <h3>{title}</h3>
          <p>{author}</p>
        </div>
        <div className="line" />
        <ArrowButton color="var(--pure-white)" background="rgba(0, 0, 0, 0)">
          <p>Read story</p>
        </ArrowButton>
      </div>
    </StoryCardStyles>
  );
}

export default StoryCard;
