import React from 'react';
import styled from 'styled-components';
import { v4 as uuid } from 'uuid';
import check from '../assets/images/pricing/desktop/check.svg';

const plans = ['basic', 'pro', 'business'];

const featuresPerPlan = [
  {
    name: 'unlimited story posting',
    basic: true,
    pro: true,
    business: true,
  },
  {
    name: 'unlimited photo upload',
    basic: true,
    pro: true,
    business: true,
  },
  {
    name: 'embedding custom content',
    pro: true,
    business: true,
  },
  {
    name: 'customize metadata',
    pro: true,
    business: true,
  },
  {
    name: 'advanced metrics',
    business: true,
  },
  {
    name: 'photo downloads',
    business: true,
  },
  {
    name: 'search engine indexing',
    business: true,
  },
  {
    name: 'custom analytics',
    business: true,
  },
];

const CompareStyles = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  padding: 10rem 3rem;
  h1 {
    text-align: center;
    margin-bottom: 2rem;
  }

  table {
    width: 70%;
    text-align: center;
    border-collapse: collapse;

    td:first-child,
    th:first-child {
      text-align: left;
    }
  }

  th,
  td {
    padding: 0.8em 1.5em;
  }

  th {
    border-bottom: 1px solid var(--pure-black);
  }

  td {
    border-bottom: 1px solid #d8d8d8;
  }
  h4 {
    text-transform: uppercase;
  }

  @media (max-width: 900px) {
    table {
      width: 90%;
    }
  }

  @media (max-width: 429px) {
    display: none;
  }
`;

// creating a new HTML bc mobile looks drastically different
const CompareStylesMobile = styled.div`
  margin: 3em auto;
  width: 90%;

  h4 {
    text-transform: uppercase;
  }

  & > h4 {
    border-bottom: 1px solid var(--pure-black);
    padding-bottom: 1.8em;
  }

  .feature {
    display: grid;
    padding: 0.8em 0;
    grid-template-columns: repeat(3, 1fr);
    border-bottom: 1px solid #d8d8d8;
    justify-content: space-between;
  }

  .feature > h4 {
    grid-column: 1 / span 3;
    padding: 1em 0;
  }

  .plan {
    h4 {
      padding-bottom: 0.5em;
    }
  }

  span {
    color: var(--dark-grey);
  }

  @media (min-width: 430px) {
    display: none;
  }
`;

function Compare() {
  return (
    <>
      <CompareStyles>
        <h1>Compare</h1>
        <table>
          <thead>
            <tr>
              <th>
                <h4>The Features</h4>
              </th>
              {plans.map((plan) => (
                <th key={uuid()}>
                  <h4>{plan}</h4>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {featuresPerPlan.map(({ name, basic, pro, business }) => (
              <tr key={uuid()}>
                <td>
                  <h4>{name}</h4>
                </td>
                <td>{basic ? <img src={check} alt="checkmark" /> : ''}</td>
                <td>{pro ? <img src={check} alt="checkmark" /> : ''}</td>
                <td>{business ? <img src={check} alt="checkmark" /> : ''}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </CompareStyles>
      <CompareStylesMobile>
        <h4>The features</h4>
        {featuresPerPlan.map((feature, index) => (
          <div className="feature" key={index}>
            <h4>{feature.name}</h4>
            {plans.map((plan) => (
              <div className="plan" key={uuid()}>
                <h4>
                  <span>{plan}</span>
                </h4>
                {feature[plan] ? <img src={check} alt="checkmark" /> : ''}
              </div>
            ))}
          </div>
        ))}
      </CompareStylesMobile>
    </>
  );
}

export default Compare;
