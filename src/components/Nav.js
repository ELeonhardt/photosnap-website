import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';
import Button from './Button';
import Logo from '../assets/images/shared/desktop/logo.svg';

const NavStyles = styled.nav`
  display: flex;
  height: 7.2rem;
  justify-content: space-around;
  align-items: center;
  position: relative;

  ul {
    display: flex;
    li {
      list-style: none;
      padding: 0 2rem;
      text-transform: uppercase;
      font-weight: 700;
      font-size: 1.2rem;
      letter-spacing: 2px;

      a {
        text-decoration: none;
        color: var(--pure-black);
      }

      a:hover {
        cursor: pointer;
        color: var(--light-grey);
        transition: all var(--transition-time);
      }
    }

    .mobile-invite-button {
      display: none;
    }
  }

  .mobile-dropdown {
    display: none;
  }

  @media (max-width: 768px) {
    ul {
      li {
        padding: 0 1rem;
      }
    }
  }

  @media (max-width: 630px) {
    .mobile-dropdown {
      display: block;
      button {
        border: none;
        background: var(--pure-white);
        cursor: pointer;
      }
    }

    ul {
      position: absolute;
      z-index: 3;
      display: flex;
      flex-direction: column;
      align-items: center;
      gap: 1rem;
      right: 0;
      top: 7.2rem;
      background: var(--pure-white);
      width: 100%;
      transform: translateX(100%);
      transition: transform var(--transition-time) ease-in-out;
      .mobile-invite-button {
        display: block;
        padding: 2rem 0;
        border-top: 1px solid var(--dark-grey);
        width: 85%;
        a {
          color: var(--pure-white);
        }
        a:hover {
          color: var(--pure-black);
        }
      }
    }

    ul.open {
      transform: translate(0%);
    }

    a {
      font-size: 1.5rem;
      font-weight: 700;
    }

    .invite {
      display: none;
    }
  }
`;

function Nav() {
  const [menu, setMenu] = useState(false);

  const closeMenu = () => setMenu(false);
  return (
    <NavStyles>
      <Link to="/">
        <img src={Logo} alt="Photosnap-logo" />
      </Link>
      <ul className={menu ? 'open' : ''}>
        <li>
          <Link onClick={closeMenu} to="/stories">
            Stories
          </Link>
        </li>
        <li>
          <Link onClick={closeMenu} to="/features">
            Features
          </Link>
        </li>
        <li>
          <Link onClick={closeMenu} to="/pricing">
            Pricing
          </Link>
        </li>
        <li className="mobile-invite-button">
          <Button clickFunction={closeMenu} color="var(--pure-white)">
            Get an Invite
          </Button>
        </li>
      </ul>
      <div className="invite">
        <Button>Get an Invite</Button>
      </div>
      <div className="mobile-dropdown">
        <button type="button" onClick={() => setMenu(!menu)}>
          {menu ? (
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="15">
              <path
                fillRule="evenodd"
                d="M14.718.075l.707.707L8.707 7.5l6.718 6.718-.707.707L8 8.207l-6.718 6.718-.707-.707L7.293 7.5.575.782l.707-.707L8 6.793 14.718.075z"
              />
            </svg>
          ) : (
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="6">
              <g fillRule="evenodd">
                <path d="M0 0h20v1H0zM0 5h20v1H0z" />
              </g>
            </svg>
          )}
        </button>
      </div>
    </NavStyles>
  );
}

export default Nav;
