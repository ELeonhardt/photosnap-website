import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import ArrowButton from './ArrowButton';

const BannerStyles = styled.div`
  min-height: 25rem;
  padding: 6rem 10vw;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  position: relative;
  color: var(--pure-white);
  align-items: center;

  .button {
    justify-self: end;
  }

  .banner-text {
    max-width: 40rem;
  }

  &:before {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    width: 6px;
    height: 100%;
    background: linear-gradient(to right top, #ffc593, #bc7198, #5a77ff);
  }

  @media (max-width: 768px) {
    padding: 6rem 4rem;
  }

  @media (max-width: 570px) {
    padding: 4rem 4rem;
    grid-template-columns: 1fr;
    grid-gap: 2em;

    .button {
      justify-self: start;
    }

    .banner-text {
      position: relative;
    }

    .banner-text:before {
      content: '';
      position: absolute;
      width: 50%;
      height: 6px;
      background: linear-gradient(to right top, #ffc593, #bc7198, #5a77ff);
      top: -4rem;
      left: 0;
    }

    &:before {
      width: 0px;
    }
  }
`;

const ImgStyles = styled(Img)`
  position: absolute !important;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: -1;
`;

function Banner({ data }) {
  const { picture, title } = data;
  return (
    <BannerStyles>
      <ImgStyles fluid={picture.asset.fluid} alt="red mountains" />
      <div className="banner-text">
        <h1>{title}</h1>
      </div>
      <div className="button">
        <ArrowButton color="var(--pure-white)" background="rgba(0, 0, 0, 0)">
          <p>Get an invite</p>
        </ArrowButton>
      </div>
    </BannerStyles>
  );
}

export default Banner;
