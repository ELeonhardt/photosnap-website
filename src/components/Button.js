import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';

const ButtonStyles = styled.button`
  background-color: ${(props) => props.background || 'var(--pure-black)'};
  color: ${(props) => props.color || 'var(--pure-white)'};
  border: none;
  text-transform: uppercase;
  font-weight: 700;
  font-size: 1.2rem;
  letter-spacing: 2px;
  padding: 1.2rem 4rem;
  transition: all var(--transition-time) ease-in-out;
  display: flex;
  justify-content: space-around;
  align-items: center;
  gap: 1.8rem;
  width: 100%;
  text-decoration: none;

  a {
    font-weight: 700;
    font-size: 1.2rem;
    letter-spacing: 2px;
    color: var(--pure-white);
  }

  &:hover {
    background-color: var(--light-grey);
    color: var(--pure-black);
    cursor: pointer;
  }
`;

function Button({ children, color, background, clickFunction }) {
  return (
    <ButtonStyles
      as={Link}
      to="/404"
      onClick={clickFunction}
      color={color}
      background={background}
    >
      {children}
    </ButtonStyles>
  );
}

export default Button;
