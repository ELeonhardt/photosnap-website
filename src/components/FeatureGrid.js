import React from 'react';
import styled from 'styled-components';
import responsive from '../assets/images/features/desktop/responsive.svg';
import noLimit from '../assets/images/features/desktop/no-limit.svg';
import embed from '../assets/images/features/desktop/embed.svg';
import customDomain from '../assets/images/features/desktop/custom-domain.svg';
import boostExposure from '../assets/images/features/desktop/boost-exposure.svg';
import dragDrop from '../assets/images/features/desktop/drag-drop.svg';

const featureDetails = [
  {
    id: 0,
    svg: responsive,
    title: '100% Responsive',
    text: `No matter which the device you're on, our site is fully responsive and
    stories look beautiful on any screen.`,
  },
  {
    id: 1,
    svg: noLimit,
    title: 'No Photo Upload Limit',
    text: `Our tool has no limits on uploads or bandwidth. Freely upload in bulk and share all of your stories in one go.`,
  },
  {
    id: 2,
    svg: embed,
    title: 'Available to Embed',
    text: `Embed Tweets, Facebook posts, Instagram media, Vimeo or YouTube videos, Google Maps, and more.`,
  },
  {
    id: 3,
    svg: customDomain,
    title: 'Custom Domain',
    text: `With Photosnap subscriptions you can host your stories on your own domain. You can also remove our branding!`,
  },
  {
    id: 4,
    svg: dragDrop,
    title: 'Boost Your Exposure',
    text: `Users that viewed your story or gallery can easily get notified of new and featured stories with our built in mailing list.`,
  },
  {
    id: 5,
    svg: boostExposure,
    title: 'Drag & Drop Image',
    text: `Easily drag and drop your image and get beautiful shots everytime. No over the top tooling to add friction to creating stories.`,
  },
];

const FeatureGridStyles = styled.section`
  max-width: 115rem;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  text-align: center;
  margin: ${(props) => `${props.margin} auto`};

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    width: 62%;
  }

  @media (max-width: 430px) {
    grid-template-columns: 1fr;
    width: 80%;
  }
`;

const FeatureStyles = styled.div`
  grid-row: span 3;
  display: grid;
  grid-template-rows: subgrid; //yeeeeaaaahhhhh
  justify-items: center;
  align-items: center;
  grid-gap: 1rem;
  padding: 3rem 1rem;

  h3 {
    margin-top: 3rem;
  }
`;

function Feature({ svg, title, text }) {
  return (
    <FeatureStyles>
      <img src={svg} alt="dd" />
      <h3>{title}</h3>
      <p>{text}</p>
    </FeatureStyles>
  );
}
function FeatureGrid({ features, margin }) {
  return (
    <FeatureGridStyles margin={margin}>
      {featureDetails.slice(0, features).map(({ id, svg, title, text }) => (
        <Feature key={id} svg={svg} title={title} text={text} />
      ))}
    </FeatureGridStyles>
  );
}

export default FeatureGrid;
