import React from 'react';
import styled from 'styled-components';
import StoryCard from './StoryCard';

const StoryCardGridStyles = styled.section`
  display: grid;
  grid-template-columns: repeat(4, 1fr);

  @media (max-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: 430px) {
    grid-template-columns: 1fr;
  }
`;

function StoryCardGrid({ data }) {
  return (
    <StoryCardGridStyles>
      {data.map((card) => (
        <StoryCard
          key={card.id}
          title={card.title}
          date={card.date}
          author={card.author}
          picture={card.picture}
        />
      ))}
    </StoryCardGridStyles>
  );
}

export default StoryCardGrid;
