import React, { useState } from 'react';
import styled from 'styled-components';
import PricingCard from './PricingCard';

const plans = [
  {
    name: 'Basic',
    description:
      'Includes basic usage of our platform. Recommended for new and aspiring photographers.',
    price: 1900,
    color: 'var(--pure-black)',
    background: 'var(--very-light-grey)',
  },
  {
    name: 'Pro',
    description:
      'More advanced features available. Recommended for photography veterans and professionals.',
    price: 3900,
    color: 'var(--pure-white)',
    background: 'var(--pure-black)',
    topBorder: true,
    height: '47rem',
  },
  {
    name: 'Business',
    description:
      'Additional features available such as more detailed metrics. Recommended for business owners.',
    price: 9900,
    color: 'var(--pure-black)',
    background: 'var(--very-light-grey)',
  },
];

const PricingGridStyles = styled.section`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  align-items: center;
  text-align: center;
  grid-gap: 2em;
  width: 85%;
  margin: auto;

  @media (max-width: 900px) {
    grid-template-columns: 1fr;
  }
`;

const ToggleButtonStyles = styled.button`
  background: var(--light-grey);
  display: inline-block;
  width: 55px;
  height: 30px;
  border-radius: 50px;
  position: relative;
  border: none;
  cursor: pointer;
  transition: background-color var(--transition-time) ease-in-out;

  &:before {
    background-color: var(--pure-black);
    content: '';
    position: absolute;
    top: 5px;
    left: 5px;
    height: 20px;
    width: 20px;
    border-radius: 20px;
    transition: transform var(--transition-time) ease-in-out,
      background-color var(--transition-time) ease-in-out;
  }

  &.yearly {
    background-color: var(--pure-black);
    transition: background-color var(--transition-time) ease-in-out;
  }

  &.yearly:before {
    background-color: var(--pure-white);
    transform: translateX(25px);
    transition: transform var(--transition-time) ease-in-out,
      background-color var(--transition-time) ease-in-out;
  }
`;

const PricingToggleStyles = styled.div`
  display: flex;
  gap: 1.5em;
  justify-content: center;
  align-items: center;
  margin: 10rem 0 5rem 0;

  p {
    font-weight: 700;
    transition: color ease-in-out var(--transition-time);
  }

  p.yearly {
    color: var(--dark-grey);
  }
`;

function PricingCards(props) {
  // true is monthly, false is yearly
  const [monthly, setPlan] = useState(true);
  return (
    <>
      <PricingToggleStyles>
        <p className={monthly ? '' : 'yearly'}>Monthly</p>
        <ToggleButtonStyles
          onClick={() => setPlan(!monthly)}
          className={monthly ? '' : 'yearly'}
        />
        <p className={monthly ? 'yearly' : ''}>Yearly</p>
      </PricingToggleStyles>
      <PricingGridStyles>
        {plans.map((plan) => (
          <PricingCard data={plan} monthly={monthly} />
        ))}
      </PricingGridStyles>
    </>
  );
}

export default PricingCards;
