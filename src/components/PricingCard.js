import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import Button from './Button';

const formatter = Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

function formatMoney(cents, monthly) {
  if (monthly) {
    return formatter.format(cents / 100);
  }
  return formatter.format((cents / 100) * 10);
}

const PricingCardStyles = styled.div`
  background: ${(props) => props.background || 'var(--light-grey)'};
  height: ${(props) => props.height || '40rem'};
  color: ${(props) => props.color || 'var(--pure-black)'};
  padding: 2.5em 2em;
  text-align: center;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  .card-content {
    display: flex;
    flex-direction: column;
    gap: 1em;
  }

  &:before {
    content: '';
    width: 100%;
    height: 6px;
    position: absolute;
    left: 0;
    top: 0;
    background: ${(props) =>
      props.topBorder
        ? 'linear-gradient(to right top, #ffc593, #bc7198, #5a77ff)'
        : props.background};
  }

  @media (max-width: 900px) {
    height: 27rem;
    .card-content {
      display: grid;
      grid-template-columns: 1fr 1fr;
      text-align: left;
      grid-gap: 2em;
    }

    .price {
      grid-column: 2;
      grid-row: 1 / 3;
      text-align: right;
    }

    &:before {
      height: 100%;
      width: 6px;
    }
  }

  @media (max-width: 500px) {
    min-height: 40rem;

    .card-content {
      grid-template-columns: 1fr;
      text-align: center;
    }

    .price {
      grid-column: auto;
      grid-row: auto;
      text-align: center;
    }

    &:before {
      width: 100%;
      height: 6px;
    }
  }
`;

function PricingCard({ data, monthly }) {
  const {
    name,
    description,
    price,
    color,
    background,
    topBorder,
    height,
  } = data;
  return (
    <PricingCardStyles
      color={color}
      background={background}
      topBorder={topBorder}
      height={height}
    >
      <div className="card-content">
        <h2>{name}</h2>
        <p>{description}</p>
        <div className="price">
          <h1>{formatMoney(price, monthly)}</h1>
          <p>{monthly ? 'per month' : 'per year'}</p>
        </div>
        <Button as={Link} to="/404" color={background} background={color}>
          Pick Plan
        </Button>
      </div>
    </PricingCardStyles>
  );
}

export default PricingCard;
