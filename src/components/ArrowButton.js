import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';

const ArrowButtonStyles = styled.button`
  background-color: ${(props) => props.background || 'var(--pure-black)'};
  color: ${(props) => props.color};
  border: none;
  text-transform: uppercase;
  transition: all 700ms ease-in-out;
  display: flex;
  justify-content: space-between;
  align-items: center;
  gap: 1.8rem;
  cursor: pointer;
  text-decoration: none;

  // in case button text sits besides an arrow
  p {
    font-weight: 700;
    letter-spacing: 2px;
    position: relative;
    font-size: 1.2rem;
  }

  p:after {
    content: '';
    display: block;
    position: absolute;
    width: 0%;
    height: 1px;
    background-color: var(--dark-grey);
    bottom: 3px;
    transition: width var(--transition-time) ease-in-out;
  }

  p:hover:after {
    width: 100%;
  }

  .arrow {
    stroke: ${(props) => props.color};
    transition: all 700ms ease-in-out;
    padding-left: 1rem;
  }
`;

function ArrowButton({ children, color, background }) {
  return (
    <ArrowButtonStyles
      as={Link}
      to="/404"
      type="button"
      color={color}
      background={background}
    >
      {children}
      <svg xmlns="http://www.w3.org/2000/svg" width="43" height="14">
        <g className="arrow" fill="none" fillRule="evenodd" stroke="#fff">
          <path d="M0 7h41.864M35.428 1l6 6-6 6" />
        </g>
      </svg>
    </ArrowButtonStyles>
  );
}

export default ArrowButton;
