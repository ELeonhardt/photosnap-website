import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import ArrowButton from './ArrowButton';
import photo from '../assets/images/home/desktop/create-and-share.jpg';

const IntroSectionStyles = styled.header`
  min-height: ${(props) => props.data.height};
  display: grid;
  grid-template-columns: ${(props) => props.data.gridTemplateColumns};
  grid-template-areas: ${(props) => props.data.gridTemplateAreas};
  background-color: ${(props) => props.data.background};
  color: ${(props) => props.data.color};

  .description {
    grid-area: 'description';
    display: flex;
    justify-content: center;
    align-items: center;

    .description-text {
      color: var(--dark-grey);
    }
  }

  .border-outer {
    border-left: ${(props) => (props.data.border ? '6px solid' : '0px solid')};
    border-image-slice: 1;
    border-image-source: linear-gradient(#5a77ff, #bc7198, #ffc593);
    width: 100%;
    display: flex;
    justify-content: center;
  }

  .text-container {
    margin: 0 2rem;
    min-height: 30rem;
    width: 38rem;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: left;
  }

  .button-wrapper {
    display: flex;
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .photo {
    grid-area: photo;
    line-height: 0;
  }

  @media (max-width: 630px) {
    grid-template-columns: 1fr;
    grid-template-areas:
      'photo'
      'description';
    img {
      height: 30rem;
    }

    h1 {
      letter-spacing: 3px;
    }

    .description {
      min-height: 42rem;
    }

    .text-container {
      width: 90%;
      gap: 2rem;
      padding: 7rem 0;
      position: relative;
    }

    .text-container:before {
      display: inline-block;
      content: '';
      position: absolute;
      width: 12.8rem;
      height: 6px;
      top: 0;
      background: ${(props) =>
        props.data.border
          ? 'linear-gradient(to right top, #ffc593, #bc7198, #5a77ff)'
          : props.data.background};
    }

    .border-outer {
      border-left: none;
      height: 100%;
    }
  }
`;

const GatsbyImageStyles = styled(Img)`
  width: 100%;
  height: 100%;
  object-fit: cover;

  @media (max-width: 630px) {
    height: 30rem;
  }
`;

function IntroSection({ data }) {
  const { headerText, paragraph, photoSrc, altText, color, background } = data;
  return (
    <IntroSectionStyles data={data}>
      <div className="description">
        <div className="border-outer">
          <div className="text-container">
            <h1>{headerText}</h1>
            <p className="description-text">{paragraph}</p>
            <div className="button-wrapper">
              <ArrowButton color={color} background={background}>
                <p>Get an Invite</p>
              </ArrowButton>
            </div>
          </div>
        </div>
      </div>
      <div className="photo">
        <GatsbyImageStyles fluid={photoSrc.asset.fluid} alt={altText} />
      </div>
    </IntroSectionStyles>
  );
}

export default IntroSection;
