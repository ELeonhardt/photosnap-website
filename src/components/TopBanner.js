import React from 'react';
import styled from 'styled-components';
import Img from 'gatsby-image';
import ArrowButton from './ArrowButton';

const IntroSectionStyles = styled.header`
  min-height: 65rem;
  display: grid;
  grid-template-columns: 1fr 1.3fr;
  color: var(--pure-white);
  position: relative;

  .description {
    display: flex;
    justify-content: center;
    align-items: center;

    .description-text,
    span {
      color: var(--dark-grey);
    }
  }

  .text-container {
    margin: 0 2rem;
    min-height: 30rem;
    width: 38rem;
    display: flex;
    flex-direction: column;
    gap: 1.5rem;
    align-items: left;
  }

  .subtitle {
    text-transform: uppercase;
    font-weight: 700;
    font-size: 1.2rem;
    letter-spacing: 2px;
  }

  .photo {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -3;
  }

  .button-wrapper {
    display: flex;
  }

  @media (max-width: 630px) {
    grid-template-columns: 1fr;
    grid-template-areas:
      'photo'
      'description';

    .photo {
      position: relative;
      grid-area: photo;
      height: 30rem;
    }

    h1 {
      letter-spacing: 3px;
    }

    .description {
      min-height: 42rem;
      background-color: var(--pure-black);
    }

    .text-container {
      width: 90%;
      gap: 2rem;
      padding: 7rem 0;
      position: relative;
    }
  }
`;

const GatsbyImageStyles = styled(Img)`
  width: 100%;
  height: 100%;

  img {
    object-position: right center !important; // !important required to overwrite the gatsby image styles
  }

  @media (max-width: 630px) {
    height: 30rem;
  }
`;

function TopBanner({ data }) {
  const { title, text, subtitle, picture, author, date } = data;
  return (
    <IntroSectionStyles data={data}>
      <div className="description">
        <div className="text-container">
          <p className="subtitle">{subtitle}</p>
          <h1>{title}</h1>
          <p>
            <span>{date}</span> by {author}
          </p>
          <p className="description-text">{text}</p>
          <div className="button-wrapper">
            <ArrowButton
              color="var(--pure-white)"
              background="rgba(0, 0, 0, 0)"
            >
              <p>Read the story</p>
            </ArrowButton>
          </div>
        </div>
      </div>
      <div className="photo">
        <GatsbyImageStyles fluid={picture.asset.fluid} />
      </div>
    </IntroSectionStyles>
  );
}

export default TopBanner;
