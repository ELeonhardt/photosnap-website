import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
:root {
    --pure-black: #000000;
    --very-light-grey: #f5f5f5;
    --light-grey: #dfdfdf;
    --dark-grey: #979797;
    --pure-white: #ffffff;

    --transition-time: 700ms;
}

* {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
}

html {
    font-family: 'DM Sans', sans-serif;
    font-size: 62.5%;
}

h1 {
    font-size: 4rem;
    line-height: 1.2;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: 4px;
}

h2 {
    font-size: 2.4rem;
    line-height: 1.1;
    font-weight: 700;
}

h3 {
    font-size: 1.8rem;
    line-height: 1.4;
    font-weight: 700;
}

h4 {
    font-size: 1.2rem;
    font-weight: 700;
    line-height: 1.3;
    letter-spacing: 2px;
}

p, div {
    font-size: 1.5rem;
    font-weight: 400;
    line-height: 1.7;
}

@media (max-width: 630px) {
    body {
        overflow-x: hidden;
    }
}

`;
export default GlobalStyles;
