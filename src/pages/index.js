import React from 'react';
import styled from 'styled-components';
import { graphql } from 'gatsby';
import FeatureGrid from '../components/FeatureGrid';
import IntroSection from '../components/IntroSection';
import StoryCardGrid from '../components/StoryCardGrid';
import SEO from '../components/SEO';

function index({ data }) {
  const sections = data.allSanityIntrosection.nodes;
  const storyCards = data.allSanityStory.nodes;
  const sectionData = [
    {
      id: sections[0].id,
      height: '65rem',
      border: true,
      background: 'var(--pure-black)',
      color: 'var(--pure-white)',
      gridTemplateAreas: `'description photo'`,
      gridTemplateColumns: '1fr 1.36fr',
      headerText: sections[0].title,
      paragraph: sections[0].text,
      photoSrc: sections[0].picture,
      altText: sections[0].title,
    },
    {
      id: sections[1].id,
      height: '60rem',
      border: false,
      background: 'var(--pure-white)',
      color: 'var(--pure-black)',
      gridTemplateAreas: `'photo description'`,
      gridTemplateColumns: '1.36fr 1fr',
      headerText: sections[1].title,
      paragraph: sections[1].text,
      photoSrc: sections[1].picture,
      altText: sections[1].title,
    },
    {
      id: sections[2].id,
      height: '60rem',
      border: false,
      background: 'var(--pure-white)',
      color: 'var(--pure-black)',
      gridTemplateAreas: `'description photo'`,
      gridTemplateColumns: '1fr 1.36fr',
      headerText: sections[2].title,
      paragraph: sections[2].text,
      photoSrc: sections[2].picture,
      altText: sections[2].title,
    },
  ];

  return (
    <>
      <SEO title="Home" />
      {sectionData.map((section) => (
        <IntroSection data={section} key={section.id} />
      ))}
      <StoryCardGrid data={storyCards} />
      <FeatureGrid features={3} margin="10rem" />
    </>
  );
}

export default index;

export const query = graphql`
  query MyQuery {
    allSanityIntrosection(
      filter: { shown: { eq: "home" } }
      sort: { order: ASC, fields: _createdAt }
    ) {
      nodes {
        id
        text
        title
        picture {
          asset {
            fluid(maxWidth: 1000) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
    allSanityStory(sort: { fields: _createdAt }, limit: 4) {
      nodes {
        id
        title
        date
        author
        picture {
          asset {
            fluid(maxWidth: 600) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
  }
`;
