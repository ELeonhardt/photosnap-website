import { graphql } from 'gatsby';
import React from 'react';
import Banner from '../components/Banner';
import Compare from '../components/Compare';
import IntroSection from '../components/IntroSection';
import PricingCards from '../components/PricingCards';
import SEO from '../components/SEO';

function pricing({ data }) {
  const sections = data.allSanityIntrosection.nodes;
  const banner = data.sanityBanner;
  const sectionData = [
    {
      id: sections[0].id,
      height: '65rem',
      border: true,
      background: 'var(--pure-black)',
      color: 'var(--pure-white)',
      gridTemplateAreas: `'description photo'`,
      gridTemplateColumns: '1fr 1.36fr',
      headerText: sections[0].title,
      paragraph: sections[0].text,
      photoSrc: sections[0].picture,
      altText: sections[0].title,
    },
  ];
  return (
    <div>
      <SEO title="Our Prices" />
      <IntroSection data={sectionData[0]} />
      <PricingCards />
      <Compare />
      <Banner data={banner} />
    </div>
  );
}

export default pricing;

export const query = graphql`
  query PricingQuery {
    allSanityIntrosection(
      filter: { shown: { eq: "pricing" } }
      sort: { order: ASC, fields: _createdAt }
    ) {
      nodes {
        id
        text
        title
        picture {
          asset {
            fluid(maxWidth: 700) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
    sanityBanner(id: { eq: "-420b9dce-56fe-5dcd-8930-7685fd2eb1d1" }) {
      _id
      title
      picture {
        asset {
          fluid(maxWidth: 1000) {
            ...GatsbySanityImageFluid
          }
        }
      }
    }
  }
`;
