import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import SEO from '../components/SEO';

const ErrorStyles = styled.div`
  min-height: calc(100vh - 32.2rem); // not really the best way, I know!
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  h2 {
    line-height: 1.7;
    width: 70%;
    padding-bottom: 4rem;
    padding-top: 2rem;
    position: relative;
  }

  .with-border:before {
    content: '';
    bottom: 0%;
    left: 0;
    position: absolute;
    width: 100%;
    height: 6px;
    background: linear-gradient(to right top, #ffc593, #bc7198, #5a77ff);
  }

  a {
    text-decoration: none;
    color: var(--dark-grey);
  }

  @media (max-width: 430px) {
    h2 {
      font-size: 1.8rem;
    }
  }
`;

function error(props) {
  return (
    <ErrorStyles>
      <SEO title="Ups! Something went wrong..." />
      <h2 className="with-border">
        Hey! I am glad you like this page! Unfortunately, it is only a
        programming challenge and not a real site… Therefore, you won’t find any
        invites, real stories or member subscriptions. You can go back to the
        <Link to="/"> main page</Link> and continue enjoying the pictures or
        check out more of my projects on my{' '}
        <a
          href="https://www.elisabethleonhardt.com"
          target="_blank"
          rel="noreferrer"
        >
          {' '}
          personal homepage
        </a>
        .
      </h2>
      <h2>
        A big shout out to{' '}
        <a
          href="https://www.frontendmentor.io/challenges"
          target="_blank"
          rel="noreferrer"
        >
          FrontendMentor
        </a>{' '}
        who made this challenge possible. You can check out the challenge here:{' '}
        <a
          href="https://www.frontendmentor.io/challenges/photosnap-multipage-website-nMDSrNmNW"
          target="_blank"
          rel="noreferrer"
        >
          See challenge
        </a>
      </h2>
    </ErrorStyles>
  );
}

export default error;
