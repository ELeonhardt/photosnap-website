import React from 'react';
import { graphql } from 'gatsby';
import FeatureGrid from '../components/FeatureGrid';
import IntroSection from '../components/IntroSection';
import Banner from '../components/Banner';
import SEO from '../components/SEO';

function features({ data }) {
  const sections = data.allSanityIntrosection.nodes;
  const banner = data.sanityBanner;
  const sectionData = [
    {
      id: sections[0].id,
      height: '65rem',
      border: true,
      background: 'var(--pure-black)',
      color: 'var(--pure-white)',
      gridTemplateAreas: `'description photo'`,
      gridTemplateColumns: '1fr 1.36fr',
      headerText: sections[0].title,
      paragraph: sections[0].text,
      photoSrc: sections[0].picture,
      altText: sections[0].title,
    },
  ];
  return (
    <div>
      <SEO title="Our Features" />
      <IntroSection data={sectionData[0]} />
      <FeatureGrid features={6} margin="13rem" />
      <Banner data={banner} />
    </div>
  );
}

export default features;

export const query = graphql`
  query FeaturesQuery {
    allSanityIntrosection(
      filter: { shown: { eq: "features" } }
      sort: { order: ASC, fields: _createdAt }
    ) {
      nodes {
        id
        text
        title
        picture {
          asset {
            fluid(maxWidth: 700) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
    sanityBanner(id: { eq: "-420b9dce-56fe-5dcd-8930-7685fd2eb1d1" }) {
      _id
      title
      picture {
        asset {
          fluid(maxWidth: 1000) {
            ...GatsbySanityImageFluid
          }
        }
      }
    }
  }
`;
