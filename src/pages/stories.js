import { graphql } from 'gatsby';
import React from 'react';
import SEO from '../components/SEO';
import StoryCardGrid from '../components/StoryCardGrid';
import TopBanner from '../components/TopBanner';

function stories({ data }) {
  const storyCards = data.allSanityStory.nodes;
  const banner = data.sanityBanner;

  return (
    <div>
      <SEO title="Uploaded Stories" />
      <TopBanner data={banner} />
      <StoryCardGrid data={storyCards} />
    </div>
  );
}

export default stories;

export const query = graphql`
  query StoriesQuery {
    allSanityStory(sort: { fields: _createdAt }) {
      nodes {
        id
        title
        date
        author
        picture {
          asset {
            fluid(maxWidth: 600) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
    sanityBanner(id: { eq: "-2e1f6331-fa84-52b7-818f-f9b4f1247407" }) {
      _id
      author
      date
      subtitle
      text
      title
      picture {
        asset {
          altText
          fluid(maxWidth: 1000) {
            ...GatsbySanityImageFluid
          }
        }
      }
    }
  }
`;
